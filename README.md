# Sticker Begone #

A chrome extension that removes all stickers from trello.

Be the change you want to see in the world.

## Instructions ##

### Installation ###

1. Checkout/download the repo
1. Open Chrome and navigate to chrome://extensions/
1. Drag the file `sticker-begone.crx` onto chrome to add the extension
1. Witness a sticker free trello

### Dev mode ###

1. Checkout the repo
1. Open chrome
1. Navigate to chrome://extensions/
1. Use the checkbox to enable Developer Mode
1. Click Load unpacked extension and select the sticker-begone directory
1. Witness a sticker free trello